var socket = io.connect(location.origin);
socket.on('news', function (data) {
console.log(data);
});
function redis(cmd,args){
  socket.emit('redisCmd',{cmd,args},x=>{
  	console.log(x)
  })
}
function output(x){
    document.getElementById('result').innerHTML=x;
}
var editor = new JSONEditor(document.getElementById('myjson'), {
  startval:{
      url:'https://ipapi.co/8.8.8.8/json/'
  },
  theme: 'bootstrap2',
  schema:{}
});

document.getElementById('previewBtn').onclick=function(){
    output('loading')
    socket.emit('preview',editor.getValue(),function(result){
        console.log(result)
    })
}
document.getElementById('submitBtn').onclick=function(){
    output('loading')
    socket.emit('submit',{
        data:editor.getValue(),
        name:'test'
    },function(result){
        console.log(result)
    })
}