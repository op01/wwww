require('dotenv').config()
var express = require('express')
var app = express()
var server = require('http').Server(app)
var io = require('socket.io')(server)
var axios = require('axios')
var crypto = require('crypto')
var Pushbullet=require('pushbullet');
var pusher = new Pushbullet(process.env.PB_TOKEN);
var Redis=require('redis')
var redisClient = Redis.createClient(process.env.REDIS_URI)
var MongoClient = require('mongodb').MongoClient
var delay=10
var port = process.env.PORT||8080
var host = process.env.IP||'0.0.0.0'
server.listen(port,host,console.log.bind(console,'listening on ',host,port))

app.use(express.static('public'))

io.on('connection',initWebsocket)
MongoClient.connect(process.env.MONGO_URI)
  .then(function (db) {
    console.log('Mongo connected')
  })
  .catch(function (err) {
    console.log(err)
  })
function sha256(x){
  var h=crypto.createHash('sha256')
  h.update(x)
  return h.digest('hex')
}
function cooldown(){
  return new Promise(res=>setTimeout(res,delay*1000))
}
var watchList=[]
function onChange(x){
   const p = new Promise((resolve,reject)=>{
    pusher.note({channel_tag:'oopbot'},'wwww',x.name,(error,response)=>{
      if(error){
        console.log(error);
        reject(error);
      }
      else{
        x.gg=true;
        resolve(response);
      }
    });
  })
  return p;
}
function watch(x){
  if(x.gg)return;
  console.log('watch',x.name)
  return axios(x.data)
  .then(y=>{
    var h=sha256(y.data.toString())
    if(x.hash===undefined || x.hash===h){
      x.hash=h;
    }
    else{
      console.log('change',x.hash,h);
      x.hash=h;
      return onChange(x)
    }
  })
}
function loop(){
  var p=Promise.resolve()
  var v;
  for(v of watchList){
    p=p.then(watch.bind(null,v))
  }
  return p.then(cooldown).then(loop);
}
loop().catch(e=>{
  console.log(e)
})
function initWebsocket(socket){
  socket.emit('news', { hello: 'world' });
  socket.on('preview',function(data,fn){
    axios(data).then(x=>{
      fn(x.data)
    })
  })
  socket.on('submit',function(data,fn){
    watchList.push(data)
    fn(data.name)
  })
  socket.on('redisCmd', function (data,fn) {
    redisClient.send_command(data['cmd'],data['args'],function(err,res){
      if(err)fn(err)
      else fn(res)
    })
  })
}